import { createApp } from 'vue';
import 'virtual:svg-icons-register';
import './style.scss';
import App from './App.vue';

createApp(App).mount('#app');
